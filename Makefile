##
# VCV Rack fedora package
#

.PHONY: srpm rpm install

rpm: vcv-rack.tar.gz
	sudo fedpkg --release f32 local

srpm: vcv-rack.tar.gz
	dnf builddep vcv-rack.spec
	fedpkg --release f32 srpm
	mv *.src.rpm $(outdir)

install: rpm
	dnf install ./x86_64/vcv-rack-1.1.6-1.fc32.x86_64.rpm

vcv-rack.tar.gz:
	dnf install -y git 'dnf-command(builddep)' fedpkg
	git submodule update --init --recursive
	cd vcv-rack && tar czf ../vcv-rack.tar.gz ./*


# end
